package ribomation.java8_course.fun_interfaces;


public class FunctionalVariations {

    interface Fun {
        String doit(String s);
    }

    public static void main(String[] args) {
        FunctionalVariations app = new FunctionalVariations();
        app.run_with_anon_class();
        app.run_with_lambda();
        app.run_with_method_ref();
    }

    void run_with_anon_class() {
        run(new Fun() {
            @Override
            public String doit(String s) {
                return s.toUpperCase();
            }
        });
    }

    void run_with_lambda() {
        run(s -> s.toUpperCase());
    }

    void run_with_method_ref() {
        run(String::toUpperCase);
    }

    void run(Fun f) {
        String x = "abcdef";
        System.out.printf("f(%s) = '%s'%n", x, f.doit(x));
    }
}
