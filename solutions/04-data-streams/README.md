Data Stream Pipelines
====

Task 1: PersonStream
----
Read a CSV file of person records (1000 lines) and filter out person objects

[Java Source: PersonStream](./src/main/java/ribomation/java8_course/data_streams/PersonStream.java)

[Java Source: Person](./src/main/java/ribomation/java8_course/data_streams/Person.java)

[Input File (1000 lines)](./src/main/resources/many-persons.csv)


Task 2: Tag Cloud
----
Read a text file (5MB), aggregate word frequencies and generate a HTMl tag cloud of the words,
where the size of each word is proportional to its frequency.

[Java Source: TagCloud](./src/main/java/ribomation/java8_course/data_streams/TagCloud.java)

[Input File (5MB)](./src/main/resources/shakespeare.txt)


Task 3: Climate Data
----
Read a GZIP:ed file (2.5GB uncompressed) of climate data records for a couple of years,
and aggregate the average temperature for each year.

[Java Source: AverageTemperature](./src/main/java/ribomation/java8_course/data_streams/AverageTemperature.java)

[Input File (433MB GZIP:ed)](https://docs.ribomation.se/java/java-8/climate-data.txt.gz)

Processing the data locally, takes ~30 seconds on my machine and loading the data 
straight from the website takes a little bit longer.

![Result of running in parallel mode](./src/assets/result.png)

![CPU Load](./src/assets/cpu-load.png)
