package ribomation.java8_course.data_streams;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

public class PersonStream {
    public static void main(String[] args) throws Exception {
        PersonStream app    = new PersonStream();
        List<Person> people = app.load("/many-persons.csv");
        people.forEach(System.out::println);
    }

    List<Person> load(String filename) throws IOException {
        BufferedReader br =
                new BufferedReader(
                        new InputStreamReader(
                                getClass().getResourceAsStream(filename)));
        return br.lines()
                .skip(1)
                .map(csv -> csv.split(","))
                //.peek(arr -> System.out.println(Arrays.toString(arr)))
                .map(fields -> new Person(fields[0], fields[1], fields[2], fields[3]))
                //.peek(System.out::println)
                .filter(Person::isFemale)  // p -> p.isFemale()
                //.peek(p -> System.out.println(p))
                .filter(p -> 30 <= p.getAge() && p.getAge() <= 40)
                .filter(p -> p.getPostCode() < 20000)
                .collect(Collectors.toList())
                //.count()
                ;
    }
}
