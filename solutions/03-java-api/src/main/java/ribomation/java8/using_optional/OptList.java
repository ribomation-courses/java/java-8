package ribomation.java8.using_optional;
import java.util.ArrayList;
import java.util.Optional;

public class OptList<T> extends ArrayList<T> {
    public Optional<T> lookup(int idx) {
        try {
            return Optional.of(super.get(idx));
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }
}
