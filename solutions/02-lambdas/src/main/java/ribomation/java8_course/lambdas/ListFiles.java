package ribomation.java8_course.lambdas;


import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ListFiles {
    public static void main(String[] args) throws Exception {
        ListFiles app = new ListFiles();
        String pkgDir = ListFiles.class.getPackage().getName().replace('.', '/');
        app.run(new File("./src/main/java/" + pkgDir));
    }

    void run(File dir) throws IOException {
        System.out.printf("DIR: %s%n", dir.getCanonicalPath());
        if (!dir.isDirectory()) {
            throw new RuntimeException("not a directory: " + dir);
        }

        File[] files = dir.listFiles(f ->
                f.isFile()
                        && f.getName().endsWith(".java")
                        && f.length() < 1010
        );
        assert files != null;
        Arrays.asList(files).forEach(System.out::println);
                                 // f -> System.out.println(f)
    }
}
