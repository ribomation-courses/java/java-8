package ribomation.java8_course.file_systems;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ScanDirs {
    public static void main(String[] args) throws Exception {
        String dirName = "../../";
        String phrase  = "Reader";

        long totalFileSize = new ScanDirs().scan(dirName, phrase);
        System.out.printf("Total size of DIR '%s' is %.2f KB",
                dirName, totalFileSize / 1024D);
    }

    long scan(String dirName, String phrase) throws IOException {
        return Files.walk(Paths.get(dirName))
                .filter(p -> Files.isRegularFile(p))
                .filter(p -> p.toString().endsWith(".java"))
                .filter(p -> sizeOf(p) > 1000)
                .filter(p -> contains(p, phrase.toLowerCase()))
                .peek(System.out::println)
                .mapToLong(p -> p.toFile().length())
                .sum()
                ;
    }

    boolean contains(Path p, String phrase) {
        try {
            return Files.lines(p)
                    .map(String::toLowerCase)
                    .anyMatch(line -> line.contains(phrase));
        } catch (IOException ignore) { }
        return false;
    }

    long sizeOf(Path p) {
        try { return Files.size(p) ; } //ALT) p.toFile().length()
        catch (IOException ignore) {  }
        return 0;
    }
}
