package ribomation.java8_course.classic_concurrency;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ComputeSize extends RecursiveTask<Result> {
    private File dir;

    public ComputeSize(File dir) {
        this.dir = dir;
    }

    @Override
    protected Result compute() {
        Result r = new Result();
        List<ComputeSize> tasks = new ArrayList<>();

        for (File f : dir.listFiles()) {
            if (f.isFile()) {
                r.numFiles++;
                r.numBytes += f.length();
            } else if (f.isDirectory()) {
                r.numDirs++;
                tasks.add((ComputeSize) new ComputeSize(f).fork());
            }
        }

       return tasks.stream()
                .map(ForkJoinTask::join)
                .collect(() -> r,
                        /*Result::add*/ (acc,elem) -> {acc.add(elem);},
                        Result::add);
    }
}
