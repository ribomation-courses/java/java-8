package ribomation.java8_course.classic_concurrency;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ForkJoinPool;

public class DirSize {
    public static void main(String[] args) throws IOException {
        String myDir = "D:\\CloudStorage\\Dropbox (Personligt)\\Ribomation\\Ribomation-Training-2017-Autumn";
        new DirSize().run(new File(myDir));
    }

    void run(File dir) throws IOException {
        System.out.printf("Scanning dir %s...%n", dir.getCanonicalFile());

        long   start  = System.nanoTime();
        Result result = new ForkJoinPool(10).invoke(new ComputeSize(dir));
        long   end    = System.nanoTime();
        System.out.printf(Locale.ENGLISH,
                "DIR: %s%n%s%nElapsed Time = %.3f secs%n",
                dir.getCanonicalPath(), result, (end - start) * 1E-9);
    }
}
