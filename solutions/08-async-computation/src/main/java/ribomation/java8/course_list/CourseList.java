package ribomation.java8.course_list;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class CourseList {
    public static void main(String[] args) {
        new CourseList().run("https://www.ribomation.se/pages/schedule.html");
    }

    void run(String url) {
        CompletableFuture.supplyAsync(() ->
                wrapUnchecked(() -> Jsoup.connect(url).get()))
                .thenApply(doc -> doc.select("#schedule td:eq(0)"))
                .thenApply(elements -> elements.stream()
                        .map(Element::text)
                        .sorted()
                        .collect(Collectors.toList()))
                .thenAccept(courses -> courses.forEach(System.out::println))
                .join();
    }

    @FunctionalInterface
    interface SupplierWithException<T> {
        T get() throws Exception;
    }

    <T> T wrapUnchecked(SupplierWithException<T> expr) {
        try {
            return expr.get();
        } catch (Exception x) {
            throw new RuntimeException(x);
        }
    }
}
