package ribomation.java8.multiple_chains;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class UsingStream extends BaseStuff {
    public static void main(String[] args) {
        new UsingStream().run();
    }

    void run() {
        long start = System.nanoTime();
        int answer = Stream.of(
                delayedValue(10, 1),
                delayedValue(4, 2),
                delayedValue(7, 2),
                delayedValue(21, 1))
                .mapToInt(CompletableFuture::join)
                .sum();
        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                answer,
                (System.nanoTime() - start) * 1E-9);
    }

}
