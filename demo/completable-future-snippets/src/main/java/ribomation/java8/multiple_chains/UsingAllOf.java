package ribomation.java8.multiple_chains;
import java.util.concurrent.CompletableFuture;

public class UsingAllOf extends BaseStuff {
    public static void main(String[] args) {
        new UsingAllOf().run();
    }

    void run() {
        CompletableFuture<Integer> chain1 = delayedValue(10, 1);
        CompletableFuture<Integer> chain2 = delayedValue(4, 1);
        CompletableFuture<Integer> chain3 = delayedValue(7, 1);
        CompletableFuture<Integer> chain4 = delayedValue(21, 1);

        long start = System.nanoTime();
        CompletableFuture.allOf(chain1, chain2, chain3, chain4).join();
        int answer = chain1.join() + chain2.join() + chain3.join() + chain4.join();
        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                answer,
                (System.nanoTime() - start) * 1E-9);
    }

}
