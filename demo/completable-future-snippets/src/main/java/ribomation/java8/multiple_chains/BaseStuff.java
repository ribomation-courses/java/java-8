package ribomation.java8.multiple_chains;
import java.util.concurrent.CompletableFuture;

public abstract class BaseStuff {
    CompletableFuture<Integer> delayedValue(int value, int numSecs) {
        return CompletableFuture.supplyAsync(() -> {
            delay(numSecs);
            return value;
        });
    }

    void delay(int numSecs) {
        try {
            Thread.sleep(numSecs * 1000);
        } catch (InterruptedException ignore) { }
    }
}
