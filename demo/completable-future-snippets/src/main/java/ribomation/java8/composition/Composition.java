package ribomation.java8.composition;
import java.util.concurrent.CompletableFuture;

public class Composition extends BaseStuff {
    public static void main(String[] args) {
        new Composition().run();
    }

    void run() {
        long start = System.nanoTime();
        CompletableFuture<Integer> g = delayedValue(21);
        CompletableFuture<Integer> f = g.thenCompose(n -> delayedValue(n, 2));
        Integer answer = f.join();
        System.out.printf("The answer is %d. Elapsed %.4f secs%n",
                answer,
                (System.nanoTime() - start) * 1E-9);
    }

    CompletableFuture<Integer> delayedValue(int value, int multiplier) {
        return CompletableFuture.supplyAsync(() -> {
            delay(1);
            return value * multiplier;
        });
    }

}
