package ribomation.java8.promises;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Fibonacci {
    public static void main(String[] args) throws Exception {
        new Fibonacci().run();
    }

    void run() throws Exception {
        ExecutorService pool = Executors.newCachedThreadPool();
        invoke(42, 5, pool);
        invoke(42, 1, pool);
        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.SECONDS);
    }

    void invoke(int arg, int timeout, ExecutorService pool) {
        System.out.printf("fibonacci(%d) ... ", arg);
        long result = compute(arg, timeout, pool);
        System.out.printf("%nfibonacci(%d) = %,d%n", arg, result);
    }

    long compute(int arg, int maxSecs, ExecutorService pool) {
        CompletableFuture<Long> promise = new CompletableFuture<>();

        pool.execute(() -> {
            long result = fibonacci(arg);
            promise.complete(result);
        });

        pool.execute(() -> {
            delay(maxSecs);
            promise.cancel(true);
        });

        try {
            return promise.join();
        } catch (CancellationException e) {
            System.out.printf("timeout: %s", e);
        }
        return 0;
    }

    long fibonacci(int n) {
        return (n <= 1) ? 1 : fibonacci(n - 2) + fibonacci(n - 1);
    }

    void delay(int numSecs) {
        try {
            Thread.sleep(numSecs * 1000);
        } catch (InterruptedException ignore) { }
    }
}
