package ribomation.java8.exceptions;

import java.util.concurrent.CompletableFuture;

public abstract class BaseStuff {
    CompletableFuture<Integer> delayedValue(int value, int numSecs) {
        return CompletableFuture.supplyAsync(() -> {
            delay(numSecs);
            return value;
        });
    }

    void delay(int numSecs) {
        try {
            Thread.sleep(numSecs * 1000);
        } catch (InterruptedException ignore) {
        }
    }

    abstract CompletableFuture<String> chain(int value);

    void run() {
        System.out.println(chain(42).join());
        System.out.println("-----");
        System.out.println(chain(41).join());
    }

}
