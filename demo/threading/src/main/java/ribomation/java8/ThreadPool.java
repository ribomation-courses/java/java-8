package ribomation.java8;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ThreadPool extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new ThreadPool().run();
    }

    void run() throws Exception {
        ExecutorService    pool   = Executors.newFixedThreadPool(cpuCount());
        BufferedReader     in     = open();
        List<Future<Long>> result = new ArrayList<>();
        for (String line; (line = in.readLine()) != null; ) {
            String       payload = line;
            Future<Long> f       = pool.submit(() -> delim.splitAsStream(payload).count());
            result.add(f);
        }
        long count = result.stream()
                .mapToLong(this::waitAndGet)
                .sum();
        print(count);
        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.SECONDS);
    }

    long waitAndGet(Future<Long> future) {
        try {
            return future.get();
        } catch (Exception ignore) {
            return 0;
        }
    }
}
