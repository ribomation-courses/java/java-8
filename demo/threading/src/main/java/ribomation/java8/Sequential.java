package ribomation.java8;

public class Sequential extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new Sequential().run();
    }

    void run() {
        long count = open()
                .lines()
                .flatMap(delim::splitAsStream)
                .count();
        print(count);
    }
}
