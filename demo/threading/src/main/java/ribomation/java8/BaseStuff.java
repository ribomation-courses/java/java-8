package ribomation.java8;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class BaseStuff {
    static final String  file  = "/shakespeare.txt";
    static final Pattern delim = Pattern.compile("[^a-zA-Z]+");

    BufferedReader open() {
        return open(file);
    }

    BufferedReader open(String path) {
        return new BufferedReader(
                new InputStreamReader(
                        getClass().getResourceAsStream(path)));
    }

    String load(BufferedReader in) {
        return in.lines().collect(Collectors.joining(" "));
    }

    int cpuCount() {
        return Runtime.getRuntime().availableProcessors();
    }

    void print(long count) {
        System.out.printf("Number of words: %,d (%s)%n", count, file);
    }

}
