package ribomation.java8;
import java.util.stream.LongStream;

public class ClassicThreads extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new ClassicThreads().run();
    }

    void run() throws Exception {
        String   payload   = load(open());
        int      N         = cpuCount();
        int      chunkSize = payload.length() / N;
        Thread[] thr       = new Thread[N];
        long[]   result    = new long[N];
        for (int k = 0; k < N; ++k) {
            int begin = k * chunkSize;
            int end   = (k + 1) * chunkSize;
            thr[k] = new WordCounter(k, result, payload.substring(begin, end));
        }
        for (Thread t : thr) t.join();
        print(LongStream.of(result).sum());
    }

    class WordCounter extends Thread {
        int    idx;
        long[] results;
        String payload;

        public WordCounter(int idx, long[] results, String payload) {
            this.idx = idx;
            this.results = results;
            this.payload = payload;
            start();
        }

        @Override
        public void run() {
            results[idx] = delim.splitAsStream(payload).count();
        }
    }

}
