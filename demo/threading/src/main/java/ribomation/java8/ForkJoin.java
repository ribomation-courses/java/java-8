package ribomation.java8;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ForkJoin extends BaseStuff {
    public static void main(String[] args) throws Exception {
        new ForkJoin().run();
    }

    void run() {
        int  chunkSize = 10 * 1024;
        long count     = new ForkJoinPool().invoke(new WordCounter(chunkSize, load(open())));
        print(count);
    }

    class WordCounter extends RecursiveTask<Long> {
        int    chunkSize;
        String payload;

        WordCounter(int chunkSize, String payload) {
            this.chunkSize = chunkSize;
            this.payload = payload;
        }

        @Override
        protected Long compute() {
            if (payload.length() <= chunkSize) {
                return delim.splitAsStream(payload).count();
            }
            int                split = payload.length() / 2;
            ForkJoinTask<Long> left  = new WordCounter(chunkSize, payload.substring(0, split)).fork();
            ForkJoinTask<Long> right = new WordCounter(chunkSize, payload.substring(split + 1)).fork();
            return left.join() + right.join();
        }
    }

}
