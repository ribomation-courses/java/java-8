package ribomation.java8.pipelines;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamToCollections {
    public static void main(String[] args) throws Exception {
        StreamToCollections app = new StreamToCollections();
        app.run();
    }

    void run() throws Exception {
        toList();
        toList2();
        toHashSet();
        toTreeSet();
    }

    final String[] names = new String[]{
            "diana", "anna", "berit", "carin", "anna",
            "carin", "diana", "berit", "anna", "carin"
    };

    void toList() {
        System.out.println("--- toList ---");
        List<String> result = Stream.of(names)
                .collect(Collectors.toList());
        System.out.println(result);
    }

    void toList2() {
        System.out.println("--- toList2 ---");
        List<String> result = Stream.of(names)
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(),
                        Collections::synchronizedList));
        System.out.println(result);
    }

    void toHashSet() {
        System.out.println("--- toSet ---");
        Set<String> result = Stream.of(names)
                .collect(Collectors.toSet());
        System.out.println(result);
    }

    void toTreeSet() {
        System.out.println("--- toTreeSet ---");
        Set<String> result = Stream.of(names)
                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(result);
    }

}
