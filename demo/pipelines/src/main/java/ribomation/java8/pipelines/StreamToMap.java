package ribomation.java8.pipelines;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamToMap {
    public static void main(String[] args) throws Exception {
        StreamToMap app = new StreamToMap();
        app.run();
    }

    void run() throws Exception {
        toBoolPartition();
        toGroup();
        toMap();
        toGroup2();
    }

    void toBoolPartition() {
        System.out.println("--- toPartition ---");
        Map<Boolean, List<Integer>> result = IntStream
                .rangeClosed(1, 42)
                .boxed()
                .collect(Collectors.partitioningBy(n -> n % 2 == 0));
        result.forEach((key, val) -> System.out.printf("%s: %s%n", key, val));
    }

    void toGroup() {
        System.out.println("--- toGroup ---");
        Map<Integer, List<Integer>> result = IntStream
                .rangeClosed(1, 42)
                .boxed()
                .collect(Collectors.groupingBy(n -> n % 5));
        result.forEach((key, val) -> System.out.printf("%s: %s%n", key, val));
    }

    void toMap() {
        System.out.println("--- toMap ---");
        Map<String, String> result = IntStream
                .rangeClosed(0, 15)
                .boxed()
                .collect(Collectors.toMap(
                        n -> "0x" + Integer.toHexString(n.intValue()),  /*key-mapper*/
                        n -> "0b" + Integer.toBinaryString(n)           /*value-mapper*/
                ));
        result.forEach((key, val) -> System.out.printf("%s: %s%n", key, val));
    }

    void toGroup2() {
        System.out.println("--- toGroup2 ---");
        Map<String, Long> result = Stream.of(names)
                .collect(Collectors.groupingBy(
                        String::toUpperCase,  /*key-mapper*/
                        Collectors.counting() /*value-mapper*/
                ));
        result.forEach((key, val) -> System.out.printf("%s: %s%n", key, val));
    }

    final String[] names = new String[]{
            "diana", "anna", "berit", "carin", "anna",
            "carin", "diana", "britta", "anna", "carin"
    };
}
