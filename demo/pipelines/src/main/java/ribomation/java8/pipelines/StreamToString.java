package ribomation.java8.pipelines;


import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamToString {
    public static void main(String[] args) throws Exception {
        StreamToString app = new StreamToString();
        app.run();
    }

    void run() throws Exception {
            toText();
            toText2();
    }

    final String[] names = new String[]{
            "diana", "anna", "berit", "carin", "anna",
            "carin", "diana", "berit", "anna", "carin"
    };

    void toText() {
        System.out.println("--- toText ---");
        String result = Stream.of(names)
                .collect(Collectors.joining("#"));
        System.out.println(result);
    }

    void toText2() {
        System.out.println("--- toText2 ---");
        String result = Stream.of(names)
                .collect(Collectors.joining(";", "<csv>", "</csv>"));
        System.out.println(result);
    }
}
