package ribomation.java8;

import java.util.Optional;
import java.util.Random;

public class UsingOptional {
    public static void main(String[] args) throws Exception {
        UsingOptional app = new UsingOptional();
        app.run();
        app.run2();
    }

    void run() throws Exception {
        repeat(5, () -> {
            Optional<String> opt = maybe();
            if (opt.isPresent()) {
                System.out.printf("has value: %s%n", opt.get());
            } else {
                System.out.printf("no value: %n");
            }
        });
    }

    void run2() throws Exception {
        System.out.println("-------");
        repeat(5, () -> {
            System.out.printf("value: %s%n", maybe().orElse("Nope"));
        });
    }

    Optional<String> maybe() {
        if (r.nextBoolean()) {
            return Optional.of("Tjabba Habba");
        } else {
            return Optional.empty();
        }
    }

    void repeat(int n, Runnable stmt) {
        for (int k = 1; k <= n; ++k) stmt.run();
    }

    Random r = new Random();
}
