package ribomation.java8;
import java.util.ArrayList;
import java.util.Optional;

public class ListWithOptional {
    public static void main(String[] args) throws Exception {
        ListWithOptional app = new ListWithOptional();
        app.run();
    }

    void run() {
        OptList<String> list = mk(5);
        System.out.printf("list[3] = %s%n", list.lookup(3).orElse("nope"));
        System.out.printf("list[7] = %s%n", list.lookup(7).orElse("nope"));
    }

    static class OptList<T> extends ArrayList<T> {
        public Optional<T> lookup(int idx) {
            try {
                return Optional.of(super.get(idx));
            } catch (IndexOutOfBoundsException e) {
                return Optional.empty();
            }
        }
    }

    OptList<String> mk(int n) {
        OptList<String> lst = new OptList<>();
        for (int k = 0; k < n; ++k) lst.add(String.format("str-%d", k));
        return lst;
    }

}
