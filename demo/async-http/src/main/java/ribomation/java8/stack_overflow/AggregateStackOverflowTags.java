package ribomation.java8.stack_overflow;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AggregateStackOverflowTags {
    public static void main(String[] args) {
        new AggregateStackOverflowTags().run();
    }

    final String tagsPath     = "/tags.txt";
    final String url          = "https://stackoverflow.com/questions/tagged/";
    final int    maxTopics    = 5;
    final String templatePath = "/result.html";
    final String title        = "Stack OverFlow Tags";
    final String name         = "Using CompletableFuture<T>";
    final String dirName      = "./stack-overflow";
    final String fileName     = "completable-future.html";

    static class Result {
        String tag;
        List<String> topics = new ArrayList<>();

        public Result(String tag) {
            this.tag = tag;
        }
    }

    void run() {
        long start = System.nanoTime();
        CompletableFuture
                .supplyAsync(() -> lines(tagsPath).map(this::fetch))
                .thenApply(promises -> promises.map(CompletableFuture::join))
                .thenApply(results -> asHTML(results, content(templatePath)))
                .thenAccept(html -> {
                    Path file = store(html, dirName, fileName);
                    System.out.printf("Written: %s%nElapsed time: %.3f seconds%n",
                            file, (System.nanoTime() - start) * 1E-9);
                })
                .exceptionally(err -> {
                    err.printStackTrace();
                    return null;
                })
                .join();
    }

    CompletableFuture<Result> fetch(String tag) {
        return CompletableFuture.supplyAsync(() -> {
            Result r = new Result(tag);
            try {
                r.topics = Jsoup
                        .connect(url + tag)
                        .get()
                        .select("a.question-hyperlink")
                        .stream()
                        .map(Element::text)
                        .limit(maxTopics)
                        .collect(Collectors.toList());
                return r;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    Stream<String> lines(String path) {
        InputStream is = this.getClass().getResourceAsStream(path);
        if (is == null) {
            throw new RuntimeException("cannot open resource " + path);
        }
        return new BufferedReader(new InputStreamReader(is))
                .lines();
    }

    String content(String path) {
        return lines(path)
                .collect(Collectors.joining(System.lineSeparator()));
    }

    Path store(String content, String dir, String file) {
        Path dirPath = Paths.get(dir);
        if (!Files.exists(dirPath)) {
            try {
                Files.createDirectory(dirPath);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        Path filePath = Paths.get(dir, file);
        try (BufferedWriter out = Files.newBufferedWriter(filePath, StandardOpenOption.CREATE)) {
            out.write(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return filePath;
    }

    String asHTML(Stream<Result> results, String template) {
        return Stream.of(template)
                .map(html -> substitute("TITLE", title, html))
                .map(html -> substitute("NAME", name, html))
                .map(html -> substitute("DATE", String.format("%1$tF %1$tR", new Date()), html))
                .map(html -> substitute("BODY", sections(results), html))
                .collect(Collectors.joining())
                ;
    }

    String sections(Stream<Result> results) {
        return results
                .map(res -> H(res.tag) + UL(res))
                .collect(Collectors.joining(System.lineSeparator()));
    }

    String H(String txt) {
        return String.format("<h3 class=\"bg-success text-white p-1\">TAG: %s</h3>%n", txt);
    }

    String UL(Result res) {
        return String.format("<ul>%n%s%n</ul>", res.topics.stream()
                .map(topic -> String.format("<li>%s</li>", topic))
                .collect(Collectors.joining(System.lineSeparator())));
    }

    String substitute(String key, String value, String doc) {
        return doc.replaceAll("\\{\\{" + key + "\\}\\}", value);
    }
}
