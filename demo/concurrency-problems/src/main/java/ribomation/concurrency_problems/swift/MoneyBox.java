package ribomation.concurrency_problems.swift;

public class MoneyBox {
    public static final int AMOUNT = 100;
    public static final int STOP = -1;
    private int payload = 0;

    public synchronized void send(int amount) {
        payload = amount;
    }
    
    public synchronized int recv() {
        return payload;
    }
}
