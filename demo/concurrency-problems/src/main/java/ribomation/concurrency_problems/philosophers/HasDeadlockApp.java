package ribomation.concurrency_problems.philosophers;

public class HasDeadlockApp extends App {
    public static void main(String[] args) {
        new HasDeadlockApp().run(args);
    }

    protected Philosopher createPhilosopher(int k) {
        final int N     = chopSticks.size();
        ChopStick left  = chopSticks.get(k % N);
        ChopStick right = chopSticks.get((k + 1) % N);

        return new Philosopher(k, left, right);
    }
}
