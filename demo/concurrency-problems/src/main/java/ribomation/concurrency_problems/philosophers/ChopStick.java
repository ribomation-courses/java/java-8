package ribomation.concurrency_problems.philosophers;

public class ChopStick {
    private boolean busy;

    public synchronized void acquire() throws InterruptedException {
        while (busy) wait();
        busy = true;
    }

    public synchronized void release() {
        busy = false;
        notifyAll();
    }
}
