package ribomation.concurrency_problems.atm;

public class UnsafeBankApp extends App {
    public static void main(String[] args) {
        new UnsafeBankApp().run(args);
    }

    @Override
    protected Account createAccount() {
        return new Account();
    }
}
