package ribomation.concurrency_problems.atm;

import java.util.ArrayList;
import java.util.List;

public abstract class App {
    private int numTransactions = 100_000;
    private int numUpdaters     = 10;
    private Account       theAccount;
    private List<Updater> updaters;

    protected abstract Account createAccount();

    protected void run(String[] args) {
        parseArgs(args);
        setup();
        showBalance();
        update();
        showBalance();
    }

    private void parseArgs(String[] args) {
        for (int k = 0; k < args.length; k++) {
            if (args[k].equals("-u")) {
                numUpdaters = Integer.parseInt(args[++k]);
            } else if (args[k].equals("-t")) {
                numTransactions = Integer.parseInt(args[++k]);
            }
        }
        System.out.printf("[bank] updaters     = %d%n", numUpdaters);
        System.out.printf("[bank] transactions = %d%n", numTransactions);
    }

    private void setup() {
        theAccount = createAccount();
        updaters = new ArrayList<>();
        for (int k = 0; k < numUpdaters; ++k) {
            updaters.add(new Updater(k + 1, theAccount, numTransactions));
        }
    }

    private void update() {
        for (Updater u : updaters) u.start();
        System.out.printf("[bank] running updates...%n");
        for (Updater u : updaters)
            try {
                u.join();
            } catch (InterruptedException ignore) { }
    }

    private void showBalance() {
        System.out.printf("[bank] balance = %d%n", theAccount.getBalance());
    }
}
