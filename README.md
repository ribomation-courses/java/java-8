Java 8, 2 days
====

Welcome to this course.
The syllabus can be found at
[java/java-8](https://www.ribomation.se/java/java-8.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to the sample project

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/java/java-8.git
    cd java-8

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have
Java JDK installed. Now, when Java 9 has been released, it's probably better
to go for the Java 9 JDK.
* [Java JDK Download](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

In addition, you need to be using a decent Java IDE, such as
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download) (_This is our choice and also used during the course_)
* [MS Visual Code](https://code.visualstudio.com/download)
* [Eclipse IDE](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/photonm3)
* [NetBeans IDE](https://netbeans.org/downloads/)

Links to Large Files
====
For some of the exercises, you might want to use a large input file. Here are some compressed large text files to use.
* [English Text, 100MB (_38MB compressed_)](https://docs.ribomation.se/java/java-8/english.100MB.gz)
* [English Text, 1024MB (_396MB compressed_)](https://docs.ribomation.se/java/java-8/english.1024MB.gz)
* [Climate Data, 2,5GB (_432MB compressed_)](https://docs.ribomation.se/java/java-8/climate-data.txt.gz)


Demo and Solution Projects
====
Each demo and solution project can be opened in Intellij, by navigating to its directory and point to the 
Gradle build file named `build.gradle`. Most other decent IDEs provide a similar service.

In order to compile a project, you don't need to install Gradle, if you don't want to. Just use the provided
wrapper instead.

    gradlew.bat build   #windows
    gradlew build       #*nix



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
